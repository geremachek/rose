pub enum RoseError {
	UnknownCommand,
	InvalidSyntax,
	StrangeArguments,
}

